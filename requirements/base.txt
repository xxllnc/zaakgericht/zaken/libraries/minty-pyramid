## Framework
pyramid~=2.0
waitress~=2.1,>=2.1.1

## Minty
minty~=3.0,>=3.0.6

python-json-logger~=2.0
jsonpath_ng~=1.5

## Other dependencies
click~=8.0
jinja2~=3.0
redis~=4.0
wrapt~=1.12
python-jose[cryptography]~=3.2
orjson~=3.6
